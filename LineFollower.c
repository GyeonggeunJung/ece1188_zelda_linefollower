#include <stdint.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/LaunchPad.h"
#include "../inc/Bump.h"
#include "../inc/SysTickInts.h"
#include "../inc/Reflectance.h"
#include "..\inc\CortexM.h"
#include "..\inc\MotorSimple.h"


uint8_t timingVar, input;
int32_t position;


void main(void){

    Clock_Init48MHz();      // running on crystal
    Motor_InitSimple();
    SysTick_Init(48000,2);  // set up SysTick for 1000 Hz interrupts
    LaunchPad_Init();       // P1.0 is red LED on LaunchPad
    EnableInterrupts();
    timingVar = 0, input = 0, position = 0;

    while(1){

        if ((position>=-4800) && (position<=4800)){
            Motor_ForwardSimple(1000,100);
        }
        else if(position > -14300 && position<-4800){  // position < -4800
            Motor_RightSimple(800, 100); // 800, 100
        }
        else{
            Motor_LeftSimple(800, 100);
        }
        Clock_Delay1us(1000000);
    }

}

void SysTick_Handler(void){

    //timing structure so that
    //I can break up the SysTick
    //Handler into 10 different parts
    if (timingVar == 9){
        timingVar = 0;
    }
    else{
        timingVar++;
    }


    //looking at timing variable
    //to decide which function to run
    switch(timingVar){

        case 8:

            Reflectance_Start();
            break;

        case 9:

            input = Reflectance_End();
            position = Reflectance_Position(input);
            break;

    }

}

